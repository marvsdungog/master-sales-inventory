<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PurchaseReceivedController extends Controller
{
    public function index()
    {
        return view('purchase.purchase_received.index');
    }
}
