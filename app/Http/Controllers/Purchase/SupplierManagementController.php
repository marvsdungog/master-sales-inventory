<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupplierManagementController extends Controller
{
    public function index()
    {
        return view('purchase.supplier_management.index');
    }
}
