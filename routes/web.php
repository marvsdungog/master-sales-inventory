<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace'=>'Dashboard'],function(){

    Route::get('/dashboard','DashboardController@index')->name('dashboard.index');
});

Route::group(['namespace'=>'PointOfSale'],function(){
    Route::get('/point-of-sale/cashier','CashierController@index')->name('cashier.index');
    Route::get('/point-of-sale/kitchen','KitchenController@index')->name('kitchen.index');
    Route::get('/point-of-sale/waiter','WaiterController@index')->name('kitchen.index');
});

Route::group(['namespace'=>'Sale'],function(){

    Route::get('/sale/invoice','InvoiceController@index')->name('invoice.index');
    Route::get('/sale/quotation','QuotationController@index')->name('quotation.index');
    Route::get('/sale/void','VoidController@index')->name('void.index');

    Route::get('/sale/daily-sales','SaleController@index')->name('daily-sales.index');
    Route::post('/sale/_load_sales','SaleController@_load_sales')->name('sales.load');
    Route::post('/sale/store','SaleController@store')->name('sales.store');
});

Route::group(['namespace'=>'Product'],function(){
    Route::get('/product/product-category','ProductCategoryController@index')->name('product-category.index');
    Route::get('/product/stock-management','StockManagementController@index')->name('stock-management.index');
    Route::get('/product/stock-adjustment','StockAdjustmentController@index')->name('stock-adjustment.index');
    Route::get('/product/defect-item','DefectItemController@index')->name('defect-item.index');
});

Route::group(['namespace'=>'customer'],function(){
    Route::get('/customer-list','CustomerController@index')->name('customer.index');
});

Route::group(['namespace'=>'purchase'],function(){
    Route::get('/purchase/supplier-management','SupplierManagementController@index')->name('supplier-management.index');
    Route::get('/purchase/purchase-order','PurchaseOrderController@index')->name('purchase-order.index');
    Route::get('/purchase/purchase-received','PurchaseReceivedController@index')->name('purchase-received.index');
    Route::get('/purchase/return-item-to-supplier','ReturnItemToSupplierController@index')->name('return-item-to-supplier.index');
});



Route::group(['namespace'=>'finance'],function(){
    Route::get('/finance','FinanceController@index')->name('finance.index');
});






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
