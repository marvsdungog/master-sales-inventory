@extends('_layouts.cashier.default-pos')

@section('content')
<div class="btn-pos ">
    <button id="kt_explore_toggle" style="display:none;" class="btn btn-sm btn-white btn-active-primary shadow-sm position-fixed px-5 fw-bolder zindex-2 top-0 left-850x mt-30 end-0 transform-0" title="" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-trigger="hover" data-bs-original-title="Explore Metronic">
        <span id="kt_explore_toggle_label">Cashier</span>
    </button>
</div>

<div class="row ">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
        <div class="row ml-4  ">
            <button type="button" class="btn btn-primary mr-2 mb-2">All</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Burger</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Coffee</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Chicken Meal</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Drinks</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Snacks</button>
            <button type="button" class="btn btn-secondary mr-2 mb-2">Salads</button>

        </div>
        <div class="row  ml-4 ">
            <div class="input-group mb-3">
                <input type="text" name="search" placeholder="search" class="form-control"><button type="button" class="btn btn-primary mr-2 mb-2">Search</button>
            </div>
        </div>
        <div class="row ml-0 ">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 text-center">
                <a class="btn btn-outline-secondary btn-menu" data-id="1">
                    <img class="img-fluid" src="http://127.0.0.1:8002/menu_images/041620210541006079236c0368a.jpeg">
                    <br>
                    Burger with Drinks &amp; Fries
                    <br>
                    <span class="badge badge-success py-1">XL</span>
                    <span class="badge badge-success py-1">Medium</span>
                    <span class="badge badge-success py-1">Small</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 text-center">
                <a class="btn btn-outline-secondary btn-menu" data-id="1">
                    <img class="img-fluid" src="http://127.0.0.1:8002/menu_images/041620210541006079236c0368a.jpeg">
                    <br>
                    Burger with Drinks &amp; Fries
                    <br>
                    <span class="badge badge-success py-1">XL</span>
                    <span class="badge badge-success py-1">Medium</span>
                    <span class="badge badge-success py-1">Small</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 text-center">
                <a class="btn btn-outline-secondary btn-menu" data-id="1">
                    <img class="img-fluid" src="http://127.0.0.1:8002/menu_images/041620210541006079236c0368a.jpeg">
                    <br>
                    Burger with Drinks &amp; Fries
                    <br>
                    <span class="badge badge-success py-1">XL</span>
                    <span class="badge badge-success py-1">Medium</span>
                    <span class="badge badge-success py-1">Small</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 text-center">
                <a class="btn btn-outline-secondary btn-menu" data-id="1">
                    <img class="img-fluid" src="http://127.0.0.1:8002/menu_images/041620210541006079236c0368a.jpeg">
                    <br>
                    Burger with Drinks &amp; Fries
                    <br>
                    <span class="badge badge-success py-1">XL</span>
                    <span class="badge badge-success py-1">Medium</span>
                    <span class="badge badge-success py-1">Small</span>
                </a>
            </div>

        </div>


    </div>
    <div class="col-xs-collapse col-sm-collapse col-md-4 col-lg-4 cashier mobile-pos sticky-top ">
            {{-- <div class="table-responsive-md" style="overflow-y:scroll; height: 380px; ">
                <table class="table table-stripped table-light">
                    <thead>
                        <tr>
                            <th scope="col">Menu</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Burger with Drinks &amp; Fries</td>
                            <td>100</td>
                            <td>
                                <div class="qty mt-5">
                                    <span class="minus ">-</span>
                                    <input type="number" class="count" name="qty" value="1">
                                    <span class="plus ">+</span>
                                </div>

                            </td>
                            <td><i class="fas fa-check-circle"></i></td>
                        </tr>

                    </tbody>
                </table>
            </div> --}}
            <div class="sticky-top mr-3">
                <div style="overflow-y:scroll; height: 300px;  ">
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>
                    <div class="order-item mt-4 ml-5 mr-7">
                        <div class="row justify-content-between font-weight-bold" >
                            <span >Granny Smitt Apples</span>
                            <span>4.89</span>
                        </div>
                        <div class="row col-8">
                            4.000kg at 1.63 /kg @ 2
                        </div>
                    </div>


                </div>


                <div id="cart-section-3" class="cashier">
                    <div class="row mx-0 px-3 py-2 font-weight-bolder border-top border-bottom">
                        <div class="col-6 p-0 text-left">
                            Sub Total
                        </div>
                    <div class="col-6 p-0 text-right">
                        $0.00
                    </div>
                </div>
                <div class="row mx-0 px-3 py-2 font-weight-bolder border-bottom">
                    <div class="col-6 p-0 text-left">
                        Tax
                    </div>
                    <div class="col-6 p-0 text-right">
                        $0.00
                    </div>
                </div>
                {{-- <div id="pop_mouse1" class="row mx-0 px-3 py-2  border-bottom">
                    <div class="col-6 p-0">
                        Discount on all items
                        <!---->
                    </div>
                    <div class="col-3 p-0 text-center">
                        <div role="group" class="btn-group dropright"></div>
                    </div>
                    <div class="col-3 p-0 text-right">
                        <span>
                            <i class="la la-edit myicon1"></i>$0.00%
                        </span>
                    </div>
                </div>
                <div id="pop_mouse2" class="row mx-0 px-3 py-2  border-bottom">
                    <div class="col-6 p-0">
                        Discount on entire Sale
                    </div>
                    <div class="col-3 p-0 text-center">
                        <div role="group" class="btn-group dropright"><!----></div>
                    </div>
                    <div class="col-3 p-0 text-right">
                        <span><i class="la la-edit myicon2"></i>$0.00
                        </span>
                    </div>
                </div> --}}
                <div class="row mx-0 px-3 py-2 border-bottom font-weight-bolder">
                    <div class="col-6 p-0 text-left">
                        <span >
                            Total
                        </span>
                        <span>
                            <span>( Tax</span>
                            <span>Excluded )</span>
                        </span>
                    </div>
                    <div class="col-3 p-0 text-center">
                        <div role="group" class="btn-group dropright"></div>
                    </div>
                    <div class="col-3 p-0 text-right">
                        <span>
                            <a data-toggle="modal" data-target="#tax-edit-modal" href="#" class="">
                                <i class="la la-edit myicon1"></i>
                            </a>
                        </span>
                        <span class="col-6 p-0 font-weight-bold text-right">
                        $0.00
                        </span>
                    </div>
                </div> <!----> <!----> <!----> <!---->
                <div class="p-3 border-bottom">
                    <button class="btn-block btn btn-primary" data-toggle="modal" data-target="#cart-payment-modal" href="#" disabled="disabled" class="btn btn-primary pay-btn app-color">
                        Pay
                    </button>
                </div>
                <div class="row mx-0 py-3 ">
                    <div class="col-6">
                        <a class="btn btn-secondary btn-block">Hold</a>
                    </div>
                    <div class="col-6">
                        <a class="btn btn-secondary btn-block">Transactions</a>
                    </div>
                </div>
                <div class="row mx-0 align-center px-3 py-4">
                    <a href="/" class="btn btn-secondary btn-block ">Exit</a>
                </div>
            </div>
    </div>
</div>

@endsection

@section('styles')
<style>
<style>
    .menu-list {
        /* border: 1px; */
        overflow-y: auto;
        margin: 0 -5px;
    }

    .cashier {
        background-color: white;
    }
    @media screen and (max-width: 736px) {
        .mobile-pos {
            display: none !important;
        }

    }

    @media screen and (max-width: 414px) {
        .btn-pos {
            visibility:visible;
        }

    }




        </style >
</style>
@endsection
