<base href="">
<meta charset="utf-8" />
    <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>
{{-- Meta Data --}}
<meta name="description" content="@yield('page_description', $page_description ?? '')"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
<meta name="csrf-token" content="{{ csrf_token() }}"

@include('_layouts.admin.base._styles')
